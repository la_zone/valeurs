# Les valeurs de « La Zone »

## Horizontalité

Nous n’apprécions pas les relations hiérarchiques, aussi nous nous abstenons d’en reproduire au sein du groupe ainsi qu’avec nos collaboratrices et collaborateurs. Malgré nos cultures et habitudes, nous tentons d’être ouvert·e·s à de nouvelles formes d’échanges.

## Écoute

Nous prêtons une oreille attentive aux membres de La Zone et au monde qui nous entoure. Nous travaillons notre empathie afin de fluidifier nos actes de collaboration. Nous prenons le temps de l’alignement entre les parties en amont d’une relation.

## Transparence

Nous souhaitons communiquer sur nos actions et rendre accessibles nos choix, nos échecs et nos réussites. Au-delà des documents produits, ce sont les processus de délibération et les outils mis en place que nous souhaitons partager afin que La Zone soit un territoire d’inspiration.

## Solidarité

Nous sommes un groupement solidaire qui met l’humain·e au cœur des décisions. Chaque membre de La Zone agit et interagit pour un pot commun contenant nos richesses (connaissances, relations, argent, etc) qui est ensuite redistribué au sein du collectif en fonction de ses besoins.