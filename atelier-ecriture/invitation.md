# Rédiger les valeurs en écriture par paire

À partir du résultat du premier [atelier sur les valeurs](https://la_zone.gitlab.io/valeurs/atelier-2108/notes-atelier.html) nous vous proposons de rédiger un paragraphe pour expliquer ce que vous imaginez derrière chaque mot-valeur.

* Horizontalité
* Écoute
* Transparence
* Solidarité

[L'exemple proposé](https://la_zone.gitlab.io/valeurs/atelier-ecriture/exercice-valeur-davidl.html) par @davidbgk

Nous vous proposons de rédiger ce document en vous inspirant et en reproduisant la pratique du ["pair-programming"](http://referentiel.institut-agile.fr/pairing.html).

## Il y a une valeur qui me gratte, et une autre qui me manque ?

Tu peux casser les consignes, hacker le bidule, proposer autre chose. C'est une proposition, tu es libre.

## Je n'ai pas participé au premier atelier, comment je peux vous rejoindre ?

Peut-être nous contacter (par exemple via slack @pntbr ou @davidbruant), nous seront ravis de trouver des solutions pour simplifier ton arrivée. Nous pouvons, par exemple, te proposer un échange en visio pour t'expliquer ce qui s'est passé. Nous pouvons également re-programmer l'atelier.

## Je ne suis pas très à l'aise à l'idée de vous rejoindre.

Beh, en vrai nous non plus. On propose de faire un truc que personne n'avait demandé. 
On pourrait peut-être commencer par échanger pour essayer de rendre la séquence plus confortable ;)