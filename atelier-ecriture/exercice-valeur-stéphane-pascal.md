# Les valeurs de « La Zone »

Les personnes présentes au démarrage de la Zone ont pris le temps de rédiger un jeu de valeur qui dévoilait l'intention du collectif.

Nous voyons nos valeurs comme un cap enthousiasmant vers lequel tendre. Dans notre fonctionnement, nous privilégions les options qui renforcent ces valeurs et nous ne sommes pas à l'aise à l'idée de les endommager.
  
## Équité - (Horizontalité)

Nous préférons l'auto-organisation à la hiérarchie.  

Nous ne sommes pas gêné par l'idée qu'une personne pilote, si et seulement si, cette posture ne s'installe que sur une petite séquence et qu'elle est consentie par l'ensemble des personnes. 

Nous reconnaissons le concept de privilège, nous pensons qu'il faut une volonté marquée et des invitations soignées pour espérer plus de disparité.  

Au même titre que la loi de la gravité, nous sommes assujetti à la loi des deux pieds. Tout est propositionnel 

- **tags** : horizontalité, collaboration, multi-disciplinarité, disparité.

## Écoute

Nous pensons que les contextes changent et que la disparité des profils facilite notre adaptation.  
Nous aimons écouter les signaux faibles, les personnes timides, atypiques, sous-représentées, etc. 
Consentement

Nous apprécions la rencontre avec d'autres contextes et d'autres cultures professionnelles. Avant de mettre en place des outils ou des méthodes nous nous assurons systématiquement du consentement de l'ensemble des parties prenantes. Ce consentement doit s'accompagner du temps de réflexion suffisant et pouvoir être remis en cause notamment lors de phases de rétrospectives.

L'amélioration continue au coeur de nos pratiques professionnelles permet un enrichissement réciproque et lémergence progressive d'une culture commune. Celle-ci ne vise pas à l'aténuation des disparités mais à l'articulation des propositions complémentaires.

- **tags** : dialogue, échange, empathie

## Transparence

La transparence autour de nos activités a pour premier objectif de faciliter l'intégration de nouvelles personnes au sein du groupement. La documentation de nos pratiques constitue la première manifestation de cet engagement.

Celui-ci doit également permettre que l'affichage de nos valeurs ne soit pas seulement un slogan mais bien une pratique. L'honnêteté intellectuelle qui guide nos activités est formalisé par la mise à disposition de nos questionnements, de compte-rendus collaboratifs et du code réalisé.

Cette transparence rejoint l'objectif de disparité dans le sens où la documentation de nos activités doit permettre l'intégration de sujets disparates au sein du groupement.

- **tags** : émancipation, sympathie, honnêteté

## Solidarité

Les personnes et les structures rassemblées au sein de la zone souhaitent développer leur solidarité au délà du formalisme du groupement solidaire. Cette solidarité peut être financière ou technique afin de faciliter la montée en compétence de l'ensemble des personnes impliquées le cas échéant.

La notion de solidarité renvoit également à un objectif de contribution aux _biens communs_ qui rejoint les orientations d'open source par défaut inscrit dans la loi pour une République numérique. 

Elle vise enfin à inscrire l'intention de transmision de nos savoir-faire et compétences à destination des différentes parties prenantes au sein des startup d'Etat, au-delà des persones impliquées dans le fonctionnement de l'incubateur.

- **tags** : bien commun, transmission, open source, utopie