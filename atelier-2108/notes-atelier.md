---
* Date : 21/08
* Présents : Nicolas, Tristram, David.L, David.B, Pntbr, Pascal
* Durée : 1h30
* Description : Atelier 1 : La scierie des pratiques
---

# Notes  

## Déroulement  
[Présentation de l'atelier](https://la_zone.gitlab.io/valeurs/atelier-2108/invitation.html)  

Clarification sur le déroulement et sur les attentes.  
- Yannick : Je suis en passager, je crois que je n'ai pas besoin de cet atelier, mais c'est ok, et je suis content de le faire.
- Pntbr : Je ne serais pas à l'aise pour démarrer un collectif sans clarifier la vision.
- David : Est-ce que l'on travaille la Vision ou est-ce qu'on pose la manière dont on va interagir ?
- Tristram : ?
- Nicolas : ?
- Pascal : ?

### Poser les valeurs
![Les premières valeurs](https://gitlab.com/la_zone/valeurs/raw/master/atelier-2108/valeurs-debut.jpg)

Les intervenants ont posé des valeurs dans un _Pad_.  
Nous avons ensuite fait un tour de parole pour expliciter les valeurs. On a changé d'approche en cours de route, on était plus à l'aise en posant des questions sur les valeurs qui ne nous semblaient pas claires.

Ex :  
- _Disparité_, est-ce que la personne qui a écrit ce mot voyait une différence avec _mixité_ ?  
- Oui, avec _mixité_ je vois un focus sur le genre. J'aurais pu mettre _diversité_, mais la connotation politique m'embêtait.

### Supprimer des valeurs
Dans cette séquence les intervenants ont retiré la moitié des valeurs. Une étape de fusion/regroupement a eu lieu, suivi d'un échange pour s'accorder sur les valeurs à garder.

![Les valeurs restantes](https://gitlab.com/la_zone/valeurs/raw/master/atelier-2108/valeurs-etape-1.jpg)

### N'en garder que trois ou quatre
 Nous avons terminé l'atelier avec les valeurs suivantes :
 
 * Horizontalité
 * Écoute
 * Transparence
 * Solidarité

### Rétro
- David L : Je trouve que ça manque de joie ou plaisir.  
- Yannick : Merc DavidL,Je me retrouve dans ce que tu dis, finalement je trouve que nos valeurs son "bateau" 
- pntbr : En 1h30 on a échangé sur les valeurs sans débattre et j'y vois un peu plus clair
- Tristram : ?
- Nicolas : ?
- Pascal : Au début ça allait, après j'ai trouvé qu'on essayait de faire rentrer les valeurs les unes dans les autres, et ça ne me semblait par pertinent.