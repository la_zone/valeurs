# Valeurs

Proposition initiale par @pntbr et @davidbruant

On ne s'attends pas à ce que cette proposition soit acceptée tel quelle. Si des personnes souhaitent revisiter ou reprendre cette proposition de zéro, c'est OK aussi.

## Intention

S'accorder sur les valeurs pour pouvoir prendre confortablement des décisions ensemble.

## Proposition

On propose la séquence suivant : 

- Ça commence lundi 20 août à 16h-UTC+1 - 10h-UTC-5 sur https://meet.jit.si/la-zone
- On va faire des ateliers à distance, en présentiel (pour ceux et celles qui voudront) et/ou un [walking-dev](http://walkingdev.fr/)
- Ça se termine par une présentation et/ou une rétro, deux semaines plus tard.
- Et après on avisera.

## Qu'est ce qu'on fait dans ces ateliers ?

On fait ce qu'on veut, et si tu n'as pas d'idée on partira sur :
- la scierie des pratiques
- l'arbrifeste
- un Walking Dev à Nantes 

## Ça dure combien de temps ces ateliers ?

La durée est libre ou contrainte.   
Par exemple, l'atelier qu'on propose de jouer lundi durera 1h30 max.

## Qui peut proposer des ateliers pendant ces deux semaine ?

Tout le monde.

## Qui peut venir ?

On propose d'inviter les personnes qui étaient engagées pendant la réponse.

L'intention est plus de dévoiler les valeurs que de les déclarer.  

## C'est quoi la scierie des pratiques ?

Un atelier jeu.

1. Chacun·e propose des valeurs - (un mot, un dessin, une expression, ...)
2. On échange pour n'en garder que la moitié
3. On répète l'étape 2, jusqu'à n'en garder qu'une ou trois ou quatre.

Les discussions permettent de s'accorder sur ce qu'on place ensemble derrière la valeur.

[scierie des pratiques](https://pablopernot.fr/pages/la-scierie-a-pratiques.html)

## Et, l'arbrifeste ?

Vu par scopyleft :
[arbrifeste](http://scopyleft.fr/blog/2014/arbrifeste-scopyleft/)

Vu par Julia Barbelane :
[Arbrifest](https://github.com/Julia-barbelane/valeursinfirst.com/blob/master/documentation/arbrifest/m%C3%A9thode.md)

En complément, une vidéo en anglais de Lyssa Adkins
[The High Performance Tree](https://www.youtube.com/watch?v=t3kKechcwYM)

## Pour la suite 

En complément on peut imaginer une résidence, par exemple, **on dispose** d'un gîte du 9 novembre au 26 novembre 2018 pouvant accueillir 25 personnes. 
