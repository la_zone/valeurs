# Dévoiler les valeurs de "la zone"

Et si on essayait de construire une intention commune ?

Du 20 août au 31 août, @davidbruant et @pntbr et qui veut, on propose de se faire des ateliers et des échanges pour clarifier nos intentions et proposer des valeurs pour "la-zone".

## Programme

Proposition pour le prochain atelier :  
https://la_zone.gitlab.io/valeurs/atelier-ecriture/invitation.html  

https://la_zone.gitlab.io/valeurs/programme.html

## Documentation

- https://www.etsu.edu/cbat/computing/seeri/documents/seeri.french.pdf - (https://www.acm.org/code-of-ethics)
- https://www.franceculture.fr/emissions/la-vie-numerique/lhistoire-du-developpeur-qui-aurait-du-lire-kant#
- http://www.internetactu.net/2018/07/19/concretement-comment-rendre-les-algorithmes-responsables-et-equitables/
